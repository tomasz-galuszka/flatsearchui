import {Component} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FilterService } from './filter.service'

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent {

	private filterForm;
	private pageLimit = 1;
	private offers = []
	private supplierPageLimits = []

	constructor(
		private formBuilder: FormBuilder,
		private service: FilterService
	) {
	    this.filterForm = this.formBuilder.group({
	      type: 'FLAT',
	      location: 'krakow',
	      priceFrom: '390000',
	      priceTo: '430000',
	      spaceFrom: '62',
	      spaceTo: '75',
	      markets: 'USED',
	      page: 1
	    });
	}

	onSubmit(data) {
	    this.service.search({...data, supplierPageLimits: this.supplierPageLimits})
	    	.subscribe(resp => {
	    			this.pageLimit = resp.pageLimit
	    			this.offers = resp.offers
	    			this.supplierPageLimits = resp.supplierPageLimits
	    	})
	}
}